package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/apache/pulsar-client-go/pulsar"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type DbPointer struct {
	Db *gorm.DB
}

type Job struct {
	ID          int    `gorm:"primary_key"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Company     string `json:"company"`
	Salary      string `json:"salary"`
}

func (Job) TableName() string {
	return "job"
}

// DBInit comment
func DBInit() *gorm.DB {

	connectionString := "root:pass@tcp(192.168.225.113:3306)/test?charset=utf8mb4&parseTime=True&loc=Local"

	db, err := gorm.Open("mysql", connectionString)
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect to database")
	}

	return db
}

func (db Job) GetUserId() int {
	return db.ID
}

func main() {

	db := &DbPointer{Db: DBInit()}

	client, err := pulsar.NewClient(pulsar.ClientOptions{URL: "pulsar://localhost:6650"})
	if err != nil {
		log.Fatal(err)
	}

	defer client.Close()

	channel := make(chan pulsar.ConsumerMessage, 100)

	options := pulsar.ConsumerOptions{
		Topic:            "topic-1",
		SubscriptionName: "my-subscription",
		Type:             pulsar.Shared,
	}

	options.MessageChannel = channel

	consumer, err := client.Subscribe(options)
	if err != nil {
		log.Fatal(err)
	}

	defer consumer.Close()

	// Receive messages from channel. The channel returns a struct which contains message and the consumer from where
	// the message was received. It's not necessary here since we have 1 single consumer, but the channel could be
	// shared across multiple consumers as well
	for cm := range channel {
		msg := cm.Message
		fmt.Printf("Received message  msgId: %v -- content: '%s'\n",
			msg.ID(), string(msg.Payload()))

		consumer.Ack(msg)
		job := string(msg.Payload())
		db.saveJobToDB(job)
	}
}

func (dbp *DbPointer) saveJobToDB(jobString string) {

	tx := dbp.Db.Begin()

	fmt.Println("Save to MongoDB")

	//Save data into Job struct
	var _job Job
	b := []byte(jobString)
	err := json.Unmarshal(b, &_job)
	if err != nil {
		panic(err)
	}

	insert := &Job{
		Title:       _job.Title,
		Description: _job.Description,
		Company:     _job.Company,
		Salary:      _job.Salary,
	}

	if err := tx.Create(&insert).Error; err != nil {
		tx.Rollback()
	}

	userId := insert.GetUserId()

	tx.Commit()

	fmt.Printf("Saved to Database : %s, record ID : %d", jobString, userId)

}
